---
linkTitle: "v5+ configuration"
title: "Asqatasun v5+ configuration"
date: 2022-03-20T08:27:13+01:00
weight: 20
---

Configuration is made through files `asqatasun-webapp.yaml` and `asqatasun-server.yaml`.

Values can be overridden following [Springboot externalized configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config)

## Database configuration

### `jdbc.url`

URL to connect to the database. Must conform to schema `jdbc:mysql://hostname:port/databaseName`

Examples:

* `jdbc:mysql://mysql.example.org:3306/asqatasunDatabase`
* `jdbc:mysql://localhost:3306/asqatasun`

### `jdbc.user`

User to connect to the database

### `jdbc.password`

Password to connect to the database

## Application configuration

### `webdriver.firefox.bin`

Full path to Firefox binary. It is not the path to the parent directory but to the binary itself.

Example: `/opt/firefox/firefox`

### `app.webapp.ui.config.webAppUrl`

Complete URL to connect to web-application. This option is needed to send info and links by email ; for instance
when a long audit is terminated, or when recovering a lost password.

Examples:

* `https://accessibility.example.org/asqatasun/`
* `https://asqatasun.example.org/`
* `http://localhost:8080/`

## Email configuration

### `app.webapp.ui.config.orchestrator.enableKrashReport`

If the application ever crashes, a message with the stack trace can be sent by email. The recipient of the message
is defined by `app.webapp.ui.config.krashReportMailList`.

Example : `true`

### `app.webapp.ui.config.orchestrator.krashReportMailList`

List of emails (comma-separated list) used to send crash reports. If `enableKrashReport` property is set
to false, this option is ignored, but has to be present, even empty

