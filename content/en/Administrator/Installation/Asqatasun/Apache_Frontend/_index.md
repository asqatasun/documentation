---
linkTitle: "Apache Frontend"
title: "Apache Frontend"
date: 2021-09-04T11:03:39+02:00
weight: 40
---

You should have already done these steps:

1. [Check Hardware provisioning](../Provisioning/)
2. [Check pre-requisites](../Pre-requisites/)
3. [Installation](../Installation/)
4. [Start Asqatasun](../Start_Asqatasun/)

(Ever need help ? Go to
[Asqatasun Forum](https://forum.asqatasun.org).)

## Pre-requisites for Apache Frontend

* Apache2
* Apache2 modules: headers, proxy, proxy_http
* Certbot 1.25+

```shell
sudo apt-get --no-install-recommends install \
    apache2
sudo a2enmod headers proxy proxy_http
```

Install Certbot following instructions from <https://certbot.eff.org/>

## Configure Apache Virtual Host

Let say we want to have Asqatasun webapp reachable through `asqatasun.example.org`.

To please Apache and be able to make some debug, we create a DocumentRoot and a single file:

```shell
sudo mkdir -p /var/www-vhosts/asqatasun.example.org
echo "Hello asqatasun.example.org" >/var/www-vhosts/asqatasun.example.org/index.html
```

Create the file `/etc/apache2/sites-available/asqatasun.example.org.conf` and add the following content:

```apacheconfig
<VirtualHost *:80>
    ServerName asqatasun.example.org
    ServerAdmin webmaster@example.org
    DocumentRoot /var/www-vhosts/asqatasun.example.org
    ServerSignature Off

    LogLevel warn
    ErrorLog ${APACHE_LOG_DIR}/asqatasun.example.org_error.log
    CustomLog ${APACHE_LOG_DIR}/asqatasun.example.org_access.log combined

    <Location />
        Require all granted
    </Location>
</VirtualHost>
```

Activate virtual host and reload Apache:

```shell script
sudo a2ensite asqatasun.example.org
sudo systemctl reload apache2
```

## Generate HTTPS certificates

```shell
sudo certbot --apache -d asqatasun.example.org
```

## Bridge Apache virtual host and Asqatasun

Certbot created a file `/etc/apache2/sites-available/asqatasun.example.org-le-ssl.conf`.
Edit this file, and just **before** the closing tag `</VirtualHost>`, add the following content:

```apacheconfig
    ## SSL directives
    SSLCertificateChainFile "/etc/letsencrypt/live/asqatasun.example.org/fullchain.pem"

    # SSL Proxy directives
    SSLProxyEngine On
    SSLProxyVerify none

    ## Request header rules
    ## as per http://httpd.apache.org/docs/2.2/mod/mod_headers.html#requestheader
    RequestHeader     set X-Forwarded-Proto https
    RequestHeader     set X-Forwarded-Port  443

    ## Proxy rules
    ProxyRequests     Off
    ProxyPreserveHost on
    ProxyPass                   /   http://127.0.0.1:8080/ Keepalive=On timeout=120
    ProxyPassReverse            /   http://127.0.0.1:8080/
    ProxyPassReverseCookiePath  /   /

    ## Security headers (enhancements from https://observatory.mozilla.org/)
    Header set X-Frame-Options DENY
    Header set X-Content-Type-Options "nosniff"
    Header set Referrer-Policy "strict-origin-when-cross-origin"
    Header set Strict-Transport-Security "max-age=63072000;"

    # BUG --> don't use "form-action" directive (bug in Chrome)
    #         see: https://gitlab.com/asqatasun/Asqatasun/-/issues/562
    Header set Content-Security-Policy "default-src 'none'; base-uri 'self'; frame-ancestors 'none'; style-src 'self' 'unsafe-inline'; script-src 'self' 'unsafe-inline' https://matomo.example.org/matomo.js ; connect-src https://matomo.example.org/matomo.php ; img-src * ; font-src 'self';  manifest-src 'self';  report-uri https://123456.report-uri-provider.com/r/d/csp/enforce ;"
```

**Caution**: do not forget to:

* replace `asqatasun.example.org` by your own domain name, especially on the last line
* replace the report-uri value endpoint with the one provided by favorite report-uri provider

```shell script
sudo systemctl reload apache2
```

## Use Asqatasun

Connect to your Asqatasun instance with the following credentials:

* URL: <https://asqatasun.example.org/>
* login: `admin@asqatasun.org`
* password: `myAsqaPassword`

This account is an administrator and has the ability to create other accounts.
