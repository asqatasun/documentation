
# ![Asqatasun]({{< baseurl >}}/images/Asqatasun_logo_RVB_crop.svg)


Asqatasun is an opensource web site analyzer, used for web accessibility (a11y) and Search Engine Optimization (SEO).

## Demo

[https://app.asqatasun.org](https://app.asqatasun.org)

[![https://app.asqatasun.org]({{< baseurl >}}/images/Screenshot_2020-08-09_Asqatasun_home_kraken.png)](https://app.asqatasun.org)

## Features

**Web accessibility assessment** `#a11y` (RGAA, WCAG)

<!-- * scan a whole site for a11y issues (crawler included) -->
* scan a given page, and manually fulfill the audit to produce report
* scan offline file (e.g. template being created but not online yet)
* scan a user-workflow like site registration, form completion or e-commerce checkout with **Asqatasun scenarios**.


## Vision

1. Automate as much as we can and even more :)
2. Be 200% reliable (don't give erroneous result)
3. have technological fun

## Installation and documentation

Online documentation: [doc.asqatasun.org](https://doc.asqatasun.org/v5/en/)

## Download

[download.asqatasun.org/](https://download.asqatasun.org/)

## Contact and discussions

* [Asqatasun forum](https://forum.asqatasun.org/)
* [Twitter @Asqatasun](https://twitter.com/Asqatasun)
* [Instant messaging on Element.io: +asqatasun:matrix.org](https://app.element.io/#/group/+asqatasun:matrix.org)
* email to `asqatasun AT asqatasun dot org` (only English, French and Klingon is spoken :) )

## Contribute

We would be glad to have you on board! You can help in many ways:

1. Use Asqatasun on your sites !
1. Give us [feedback on the forum](https://forum.asqatasun.org)
1. [Fill in bug report](https://gitlab.com/Asqatasun/Asqatasun/issues)
1. [Contribute](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CONTRIBUTING.md) code

---

## License

[AGPL v3](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/LICENSE)


## Have Fun

Happy testing !

[Asqatasun Team](https://gitlab.com/Asqatasun/Asqatasun/blob/master/documentation/en/asqatasun-team.md)


