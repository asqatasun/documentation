---
linkTitle: "Components"
title: "Engine Components"
date: 2021-09-03T20:29:13+02:00
weight: 10
---

Main componants of the Asqatasun Engine (logical view)

1. Crawler
1. Adapter
1. Processor
1. Consolidator
1. Analyser

![Asqatasun Engine overview](./asqatasun_engine_overview.png)
[Asqatasun Engine overview (png, 51 Kb)](./asqatasun_engine_overview.png)
