---
linkTitle: "Data model v4"
title: "Data model for Asqatasun v4"
date: 2022-03-25T15:46:34+01:00
weight: 10
---

## Data model for the *Engine* part

Download the [Data Model in Mysql WorkBench format (28kb)](./Asqatasun_Data_model_v3.0.mwb)

Data model for releases 3.x and further:

![Data Model](./Asqatasun_Data_model_v3.0.png)
[Data Model (png, 670 kb)](./Asqatasun_Data_model_v3.0.png)

## Data model for the *Information System* part

Download the
[*Information System* Data Model in Mysql WorkBench format (17kb)](./Asqatasun_Information_System_Data_model.mwb)

*Information System* Data model for releases 3.x and further:

![Data Model](./Asqatasun_Information_System_Data_model.png)
[Data Model (png, 330 Kb)](./Asqatasun_Information_System_Data_model.png)
