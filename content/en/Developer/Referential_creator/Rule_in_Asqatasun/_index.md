---
title: "Rule in Asqatasun"
date: 2021-09-03T21:25:28+02:00
weight: 30
---

## Rules input/output

@@@TODO refactor this doc

### Input

* DOM
* CSS "XML-ised"

@@@to be completed with more details

### Output

A test can produce a result with 3 levels of information:

* Level1 (mandatory) -> PROCESS_RESULT that handles the final result of the test
  (and the total number of elements implied by the test)
* Level2 (mandatory) -> PROCESS_REMARK that can be either a general message associated with the result,
  or a message associated with an element of the DOM that needs to be treated. In this case, the type of the element,
  its result regarding the test, its position (line number), the source code representing it are automatically saved.
* Level3 (optional) -> EVIDENCE_ELEMENT that can store additional information about the DOM element to help the
  qualification and thus the resolution.

## The main interfaces

### The `ElementSelector`

#### The `ElementSelector` > Explanation

This interface defines a selection applied to the DOM, to set the scope of the rule, and thus its applicability

#### The existing ElementSelector implementations

Here is a not exhaustive list of existing ElementSelector implementations:

* [SimpleElementSelector](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementselector/SimpleElementSelector.java)
* [MultipleElementSelector](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementselector/MultipleElementSelector.java)
* [LinkElementSelector](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementselector/LinkElementSelector.java)
* [ImageElementSelector](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementselector/ImageElementSelector.java)
* ...

### The `ElementChecker`

#### The `ElementChecker` > Explanation

This interface defines a check to be done on elements

#### The `ElementChecker` > Method to implement

```java
/**
     * Perform the check operation. The instance of {@link ElementHandler}
     * received as a parameter is used to retrieve elements the test is about
     * and the instance of {@link TestSolutionHandler} received
     * as a parameter is used to store the results of tests performed
     * during the operation
     *
     * @param sspHandler
     * @param elementHandler
     * @param testSolutionHandler
     *
     */
    void check (
            SSPHandler sspHandler,
            ElementHandler elementHandler,
            TestSolutionHandler testSolutionHandler);
```

#### Abstract implementation

#### The existing ElementChecker implementations

Here is a not exhaustive list of existing ElementChecker implementations:

* [ElementPresenceChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/element/ElementPresenceChecker.java)
* [ChildElementPresenceChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/element/ChildElementPresenceChecker.java)
* [ElementUnicityChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/element/ElementUnicityChecker.java)
* [AttributePresenceChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/element/ElementWithAttributePresenceChecker.java)
* [TextEmptinessChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/text/TextEmptinessChecker.java)
* [TextLengthChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/text/TextLengthChecker.java)
* [TextBelongsToBlackListChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/text/TextBelongsToBlackListChecker.java)
* [DoctypeValidityChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/doctype/DoctypeValidityChecker.java)
* [HeadingsHierarchyChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/headings/HeadingsHierarchyChecker.java)
* [LangChangeChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/lang/LangChangeChecker.java)
* [LinkPertinenceChecker](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/elementchecker/link/LinkPertinenceChecker.java)
* ...

### The `TextElementBuilder`

#### The `TextElementBuilder` > Explanation

This builder is in charge of creating a textual representation of an HTML element.

#### The `TextElementBuilder` > Method to implement

```java
/**
 * @param element
 * @return a textual representation of the element
 */
String buildTextFromElement(Element element);
```

Take a look at the [online javadoc of the Jsoup Element](https://jsoup.org/apidocs/org/jsoup/nodes/Element.html).

#### The existing TextElementBuilder implementations

Here is the list of existing TextElementBuilder implementations:

* [SimpleTextElementBuilder](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/textbuilder/SimpleTextElementBuilder.java)
* [OwnTextElementBuilder](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/textbuilder/OwnTextElementBuilder.java)
* [TextAttributeOfElementBuilder](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/textbuilder/TextAttributeOfElementBuilder.java)
* [DeepTextElementBuilder](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/textbuilder/DeepTextElementBuilder.java)
* [CompleteTextElementBuilder](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/textbuilder/CompleteTextElementBuilder.java)
* [LinkTextElementBuilder](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/textbuilder/LinkTextElementBuilder.java)
* [PathElementBuilder](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-commons/src/main/java/org/asqatasun/rules/textbuilder/PathElementBuilder.java)

## Test context

### Create a nomenclature and populate it

```mysql
-- INSERT THE NOMENCLATURE
INSERT IGNORE INTO `NOMENCLATURE` (`Cd_Nomenclature`) VALUES ('MyNomenclature');

-- INSERT AN ELEMENT AND LINK IT TO THE NOMENCLATURE
INSERT IGNORE INTO `NOMENCLATURE_ELEMENT` (`DTYPE`, `Label`) VALUES ('NomenclatureElementImpl', 'Value1');
UPDATE `NOMENCLATURE_ELEMENT`
SET `Id_Nomenclature`= (
    SELECT `Id_Nomenclature`
    FROM `NOMENCLATURE`
    WHERE `Cd_Nomenclature`
    LIKE 'MyNomenclature')
WHERE Label like 'Value1';
```

