---
title: "Link checking"
date: 2021-09-02T20:13:55+02:00
weight: 20
---

## Check links locally

### With filiph/LinkCheck

We tend to use [filiph/LinkCheck](https://github.com/filiph/linkcheck/) to verify links.
Two docker images are available: the [official one](https://github.com/filiph/linkcheck/blob/master/Dockerfile)
and [the one we created](https://gitlab.com/asqatasun/linkcheck-docker) for testing purpose

```shell script
linkcheck http://localhost:1313/en/
```

### With Remark-validate-links

To check internal links directly in the markdown files (which is faster when you write doc, and easier as you have the correct
line number), we use [Remark-validate-links](https://github.com/remarkjs/remark-validate-links), which is in fact
a plugin for RemarkJS.

Local installation: `sudo npm install --global remark-cli remark-validate-links`

To check links locally (from the root of the repository):

```shell script
remark -u validate-links content/
```

## Check links on STAGING <https://asqatasun.gitlab.io/documentation/>

```shell script
linkcheck -e --skip-file .gitlab/config/linkcheck-skip-STAGING.txt --no-check-anchors  https://asqatasun.gitlab.io/documentation/en/
```


## Check links on PRODUCTION <https://doc.asqatasun.org/>

```shell script
linkcheck -e --skip-file .gitlab/config/linkcheck-skip-PROD.txt --no-check-anchors  https://doc.asqatasun.org/v5/en/
```

## Check links with Gitlab-CI

Work is in progress: [Implement link validation with filiph/LinkCheck](https://gitlab.com/asqatasun/documentation/-/issues/85)
