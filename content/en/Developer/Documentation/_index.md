---
linkTitle: "Documentation"
title: "Asqatasun Documentation"
date: 2020-08-09T09:33:01+02:00
weight: 80
---

## References

* Documentation webite is [https://doc.asqatasun.org](https://doc.asqatasun.org)
* Repository for this website is [https://gitlab.com/asqatasun/documentation](https://gitlab.com/asqatasun/documentation)

## Organisation of doc.asqatasun.org

* Documentation website is a static site made with Hugo.
* The theme used is [Learn](https://github.com/matcornic/hugo-theme-learn/)
* Old doc website (gitbook) is copied in directory `zz-old-gitbook-documentation` for historical purpose.

## How to clone this repository (beware: submodule inside)

Directory `themes/learn/` is a git submodule.

So to clone the repository for **the very first time**, do:

```shell script
git clone --recurse-submodules git@gitlab.com:asqatasun/documentation.git
```

If you **already cloned** the repos, but the submodule is not present, do:

```shell script
cd themes/learn/
git submodule update --init --recursive
```

Reference: [How to “git clone” including submodules?](https://stackoverflow.com/questions/3796927/how-to-git-clone-including-submodules)
