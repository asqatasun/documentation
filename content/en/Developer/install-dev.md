---
title: "Install Dev"
date: 2020-08-29T14:04:00+02:00
---

These are the build instructions for Asqatasun v5 and onward.

## 1. Pre-requisites

Pre-requisites are:

* Java8
* Firefox ESR 68 (version *is* important)
* GeckoDriver
* a MariaDB container

```shell script
sudo apt-get install openjdk-8-jdk
cd /opt
sudo wget https://download-installer.cdn.mozilla.net/pub/firefox/releases/68.9.0esr/linux-x86_64/en-US/firefox-68.9.0esr.tar.bz2
sudo tar xvf firefox-68.9.0esr.tar.bz2
sudo wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz
sudo tar xvf geckodriver-v0.26.0-linux64.tar.gz
docker run --name asqatasun-db -e MYSQL_ROOT_PASSWORD=bou -e MYSQL_DATABASE=asqatasun -d -p 3306:3306 mariadb:10.2
```

## 2. Build locally

Grab source code and build locally

```shell script
git clone https://gitlab.com/Asqatasun/Asqatasun.git
cd Asqatasun
# TEMPORARY NOTE: use the branch "294-..." until it is merged into "v5"
git checkout 294_Use_selenium_IDE_to_create_and_replay_scenario
mvn clean install
```


## 3. Launch application

```shell script
PATH_TO_ASQATASUN_SOURCE_DIR=.
java \
  -Dwebdriver.firefox.bin=/opt/firefox/firefox \
  -Dwebdriver.gecko.driver=/opt/geckodriver \
  -jar $PATH_TO_ASQATASUN_SOURCE_DIR/web-app/asqatasun-web-app/target/asqatasun-web-app-5.0-SNAPSHOT.war \
  --server.port=5959
```

Once you see something like:

```log
29-08-2020 15:33:10:852 25086 INFO  org.asqatasun.webapp.AsqatasunWebappApplication - Started AsqatasunWebappApplication in 24.548 seconds (JVM running for 25.642)
```

... you can browse <http://localhost:5959>
