---
title: "Producing a Release"
date: 2020-11-12T10:34:32+01:00
weight: 20
---

Here are described the steps to produce a release

## 0. Create the *release checklist* issue

Copy / paste the [Definition of Done](../Release-definition-of-done/) into an issue.

## 1. Assumptions

* `${RELEASE_VERSION}` is the version string we're about to release. It must be SemVer compatible. Example:
  `5.0.0-rc.2`
* `${DEV_VERSION}` is the version string of the code base:
    * *after* the release is created,
    * and for the immediate next release.
    * (It is not considered as a version in the sense of SemVer.)
    * Example: `5.0.0-SNAPSHOT`

```shell script
RELEASE_VERSION="5.0.0-alpha.2"
DEV_VERSION="5.0.0-SNAPSHOT"
```

## 2. Update `CHANGELOG.md`

## 3. Update `README.md`

## 4. Update `contributors.txt`

From the root of the repos:

```shell script
git checkout master
engine/asqatasun-resources/src/main/resources/release/update_contributors.sh --to-version ${RELEASE_VERSION}
git add contributors.txt
```

**Note**: the `update_contributors.sh` script relies on a `.mailmap` file, that groups various names / emails of
a given contributor into a unique one (couple name/email). As the `.mailmap` file contains a lot of personal emails
we chose deliberately *not* to place it in the repository.

## 5. Release version

We work on branch `master` (this will be enhanced later; all leads are stored
in [issue #494 + comments](https://gitlab.com/asqatasun/Asqatasun/-/issues/494)).

```shell script
engine/asqatasun-resources/src/main/resources/release/bump_asqatasun_v5.sh \
    --release-version "${RELEASE_VERSION}" \
    --dev-version "${DEV_VERSION}"
```

## 6. Upload artifact to Gitlab package repository

Resources:

- You need a *deploy* token
  [configured in the Asqatasun Gitlab's project](https://gitlab.com/asqatasun/Asqatasun/-/settings/access_tokens)
- You may want to read [Gitlab doc on
  *Generic* Package Registry](https://docs.gitlab.com/ee/user/packages/generic_packages/#gitlab-generic-packages-repository)

```shell
VERSION="5.0.0-rc.2"
MYTOKEN="my-API-token"

# API Rest server
curl --header "PRIVATE-TOKEN: ${MYTOKEN}" \
  --upload-file asqatasun-server-${VERSION}.jar \
  "https://gitlab.com/api/v4/projects/20815843/packages/generic/Asqatasun/${VERSION}/asqatasun-server-${VERSION}.jar"
# Webapp
curl --header "PRIVATE-TOKEN: ${MYTOKEN}" \
  --upload-file asqatasun-web-app-${VERSION}.war \
  "https://gitlab.com/api/v4/projects/20815843/packages/generic/Asqatasun/${VERSION}/asqatasun-web-app-${VERSION}.war"
```

## 7. Create Docker images

Create docker images for that version on repository [Asqatasun-docker](https://gitlab.com/asqatasun/asqatasun-docker/).

Leverage `tools/generator/generator.sh` to automatically create three images.

## 8. Add build artefact to the Gitlab release

### Download release-cli

```shell
sudo curl --location --output /usr/local/bin/release-cli "https://gitlab.com/api/v4/projects/gitlab-org%2Frelease-cli/packages/generic/release-cli/latest/release-cli-linux-amd64"
sudo chmod +x /usr/local/bin/release-cli
release-cli -v
```

### Create the release

```shell
VERSION="5.0.0"
RELEASE_DATE="2023-10-15T00:00:00Z"
TOKEN="my-API-token"
GIT_TAG="$VERSION"
MILESTONE="v${VERSION}"

release-cli \
  --server-url "https://gitlab.com" \
  --private-token="$TOKEN" \
  --project-id 20815843 \
  create \
  --name "Asqatasun $VERSION" \
  --description "Description" \
  --tag-name $GIT_TAG \
  --assets-link '{"name":"Asqatasun web application","url":"https://gitlab.com/api/v4/projects/20815843/packages/generic/Asqatasun/'${VERSION}'/asqatasun-web-app-'${VERSION}'.war","link_type":"package"}' \
  --assets-link '{"name":"Asqatasun server (REST API server)","url":"https://gitlab.com/api/v4/projects/20815843/packages/generic/Asqatasun/'${VERSION}'/asqatasun-server-'${VERSION}'.jar","link_type":"package"}' \
  --milestone "$MILESTONE" \
  --released-at "$RELEASE_DATE"
```

### Add content to the release

Once created, edit the release and add content:

* Docker images: add link to the suitable directory in the Asqatasun Docker repository
* Copy/paste content of changelog for the given version

### Further work

For now, we do this more or less manually. Later, we could leverage `release-cli` for automation in CI.
See:

* Gitlab doc [release CI/CD examples](https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html)
* [release-cli doc](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs#using-this-tool-in-gitlab-ci)
