---
linkTitle: "Build CLI"
title: "Build with Command Line Interface"
date: 2022-03-30T18:35:09+02:00
weight: 12
---

(All this information is based on [what the CI does](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/.gitlab-ci.yml))

{{% notice info %}}
We assume the current working directory is where Asqatasun source code is.
For instance: `/home/asqatasun/Asqatasun/`
{{% /notice %}}

## Prepare and build

```shell
MAVEN_CLI_OPTS="--batch-mode"
MAVEN_OPTS="-Dmaven.repo.local=.m2/repository"
./mvnw $MAVEN_CLI_OPTS validate
./mvnw $MAVEN_CLI_OPTS clean
./mvnw $MAVEN_CLI_OPTS install  -Dmaven.test.skip=true
./mvnw $MAVEN_CLI_OPTS test
sudo mkdir -p \
  /var/log/asqatasun/webapp/ \
  /var/log/asqatasun/server/
```

## Run webapp

```shell
APP_PORT="8080"
DB_PORT="3307"
DB_HOST="localhost"
DB_DRIVER="mysql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="/var/log/asqatasun/webapp"
WAR_FILE="/home/asqatasun/Asqatasun/web-app/asqatasun-web-app/target/asqatasun-web-app-5-SNAPSHOT.war"

sudo /usr/bin/java \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \
    -Dwebdriver.gecko.driver=/opt/geckodriver     \
    -Dapp.logging.path="${APP_LOG_DIR}"           \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar "${WAR_FILE}"
```

## Run server

```shell
APP_PORT="8080"
DB_PORT="3307"
DB_HOST="localhost"
DB_DRIVER="mysql"
DB_DATABASE="asqatasun"
DB_USER="asqatasunDatabaseUserLogin"
DB_PASSWORD="asqatasunDatabaseUserP4ssword"
APP_LOG_DIR="/var/log/asqatasun/server"
JAR_FILE="/home/asqatasun/Asqatasun/server/asqatasun-server/target/asqatasun-server-5-SNAPSHOT.jar"

sudo /usr/bin/java \
    -Dwebdriver.firefox.bin=/opt/firefox/firefox  \
    -Dwebdriver.gecko.driver=/opt/geckodriver     \
    -Dapp.logging.path="${APP_LOG_DIR}"           \
    -Djdbc.url="jdbc:${DB_DRIVER}://${DB_HOST}:${DB_PORT}/${DB_DATABASE}" \
    -Djdbc.user="${DB_USER}" \
    -Djdbc.password="${DB_PASSWORD}" \
    -jar "${JAR_FILE}"
```

## Stop service

Just run a `CTRL-C`.

## Use service

For both services, browse <http://localhost:8080/> and connect with credentials:

- login: `admin@asqatasun.org`
- password: `myAsqaPassword`

{{% notice info %}}
For Asqatasun Server, in the Swagger UI, you must change the value of `contractId` from `0` to `1`
or your request will end in a 500 error.
{{% /notice %}}
