---
linkTitle: "Pre-requisites"
title: "Pre-requisites for development"
date: 2021-09-25T12:08:54+02:00
weight: 10
---

We describe how to create a developer environment to build Asqatasun locally
on an **Ubuntu 22.04** / **Linux Mint 21**

## Pre-requisites

* Maven 3.6
* jdk8
* MariaDB 10.6
* Firefox + Gecko Driver

### Jdk 8

```bash
sudo apt install openjdk-8-jdk
sudo update-alternatives --config java
java -version
```

### Maven

```bash
sudo apt install maven
```

### Firefox + Gecko Driver

```shell script
FIREFOX_VERSION="102.8.0esr"
GECKODRIVER_VERSION="v0.32.2"
FIREFOX_URL_PREFIX="https://download-installer.cdn.mozilla.net/pub/firefox/releases/"
GECKODRIVER_URL_PREFIX="https://github.com/mozilla/geckodriver/releases/download/"
FIREFOX_URL="${FIREFOX_URL_PREFIX}${FIREFOX_VERSION}/linux-x86_64/en-US/firefox-${FIREFOX_VERSION}.tar.bz2"
GECKODRIVER_URL="${GECKODRIVER_URL_PREFIX}${GECKODRIVER_VERSION}/geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"

cd /opt
sudo wget "${FIREFOX_URL}"
sudo wget "${GECKODRIVER_URL}"
sudo tar xf "firefox-${FIREFOX_VERSION}.tar.bz2"
sudo tar xf "geckodriver-${GECKODRIVER_VERSION}-linux64.tar.gz"
```

**Notes**:

* It is important to stick to the specified version of Firefox.
* Firefox version and GeckoDriver version are strongly tied:
  see [table on GeckoDriver documentation](https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html)

### MariaDB

We decide to use a Docker container

```shell script
docker run \
  --name mysql \
  -e MYSQL_ROOT_PASSWORD=my-secret-pw \
  -e MYSQL_USER=asqatasunDatabaseUserLogin \
  -e MYSQL_PASSWORD=asqatasunDatabaseUserP4ssword \
  -e MYSQL_DATABASE=asqatasun \
  -p 3307:3306 \
  -d mariadb:10.6
```

Notes:

* In this snippet, we choose to launch Mysql on port `3307` in case another Mysql is already running on `3306`
* The values `asqatasunDatabaseUserLogin`, `asqatasunDatabaseUserP4ssword` and `asqatasun` are the default ones
  and defined in [`application.yml` (webapp)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/web-app/asqatasun-web-app/src/main/resources/application.yml#L4)
  and [`application.yml` (API)](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/server/asqatasun-server/src/main/resources/application.yml#L4)

## Get Asqatasun source code

```shell script
git clone https://github.com/Asqatasun/Asqatasun.git
cd Asqatasun
```
