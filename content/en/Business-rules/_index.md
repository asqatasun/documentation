---
title: "Business rules manual"
date: 2020-11-12T17:14:53+02:00
weight: 40
---

Referential insights:

* [RGAA in Asqatasun](RGAA_in_asqatasun)

Business rules manual:

* [RGAA 4]({{< relref "./RGAA-v4/" >}})
* [RGAA 3]({{< relref "./RGAA-v3/" >}})

