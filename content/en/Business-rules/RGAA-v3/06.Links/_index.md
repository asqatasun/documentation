---
title: "RGAA 3.0 Theme 6: Links"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Criterion 6.1

Is each link explicit (except in particular cases)?

* [Rule 6.1.1]({{< relref "./Rule-6-1-1" >}})
* [Rule 6.1.2]({{< relref "./Rule-6-1-2" >}})
* [Rule 6.1.3]({{< relref "./Rule-6-1-3" >}})
* [Rule 6.1.4]({{< relref "./Rule-6-1-4" >}})
* [Rule 6.1.5]({{< relref "./Rule-6-1-5" >}})

## Criterion 6.2

For each link with a link title, is this title relevant?

* [Rule 6.2.1]({{< relref "./Rule-6-2-1" >}})
* [Rule 6.2.2]({{< relref "./Rule-6-2-2" >}})
* [Rule 6.2.3]({{< relref "./Rule-6-2-3" >}})
* [Rule 6.2.4]({{< relref "./Rule-6-2-4" >}})
* [Rule 6.2.5]({{< relref "./Rule-6-2-5" >}})

## Criterion 6.3

Is each link text alone explicit out of context (except in particular cases)?

* [Rule 6.3.1]({{< relref "./Rule-6-3-1" >}})
* [Rule 6.3.2]({{< relref "./Rule-6-3-2" >}})
* [Rule 6.3.3]({{< relref "./Rule-6-3-3" >}})
* [Rule 6.3.4]({{< relref "./Rule-6-3-4" >}})
* [Rule 6.3.5]({{< relref "./Rule-6-3-5" >}})

## Criterion 6.4

For each web page, does each identical link have the same purpose and target?

* [Rule 6.4.1]({{< relref "./Rule-6-4-1" >}})
* [Rule 6.4.2]({{< relref "./Rule-6-4-2" >}})
* [Rule 6.4.3]({{< relref "./Rule-6-4-3" >}})
* [Rule 6.4.4]({{< relref "./Rule-6-4-4" >}})
* [Rule 6.4.5]({{< relref "./Rule-6-4-5" >}})

## Criterion 6.5

On each Web page, does each link, except in anchors, have a text?

* [Rule 6.5.1]({{< relref "./Rule-6-5-1" >}})
