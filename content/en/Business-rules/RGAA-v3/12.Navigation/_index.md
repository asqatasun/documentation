---
title: "RGAA 3.0 Theme 12: Navigation"
date: 2020-11-15T18:18:18+02:00
weight: 15
---

## Criterion 12.1

Does each set of pages have at least two different
navigation systems (except in particular cases)?

* [Rule 12.1.1]({{< relref "./Rule-12-1-1" >}})

## Criterion 12.2

On each set of pages, are the menu or the navigation bars
always located at the same place (except in particular cases)?

* [Rule 12.2.1]({{< relref "./Rule-12-2-1" >}})
* [Rule 12.2.2]({{< relref "./Rule-12-2-2" >}})

## Criterion 12.3

On each set of pages, do the menu and the navigation bars
have a consistent presentation (except in particular cases)?

* [Rule 12.3.1]({{< relref "./Rule-12-3-1" >}})
* [Rule 12.3.2]({{< relref "./Rule-12-3-2" >}})

## Criterion 12.4

Is the "site map" page relevant?

* [Rule 12.4.1]({{< relref "./Rule-12-4-1" >}})
* [Rule 12.4.2]({{< relref "./Rule-12-4-2" >}})
* [Rule 12.4.3]({{< relref "./Rule-12-4-3" >}})

## Criterion 12.5

In each set of pages, is the "site map"
page accessible in an identical way?

* [Rule 12.5.1]({{< relref "./Rule-12-5-1" >}})
* [Rule 12.5.2]({{< relref "./Rule-12-5-2" >}})
* [Rule 12.5.3]({{< relref "./Rule-12-5-3" >}})

## Criterion 12.6

In each set of pages, is the search
function accessible in an identical way?

* [Rule 12.6.1]({{< relref "./Rule-12-6-1" >}})
* [Rule 12.6.2]({{< relref "./Rule-12-6-2" >}})
* [Rule 12.6.3]({{< relref "./Rule-12-6-3" >}})

## Criterion 12.7

On each page within a collection of pages,
are links facilitating navigation available?

* [Rule 12.7.1]({{< relref "./Rule-12-7-1" >}})

## Criterion 12.8

On each Web page, is a breadcrumb trail
available (except in particular cases)?

* [Rule 12.8.1]({{< relref "./Rule-12-8-1" >}})

## Criterion 12.9

On each Web page, is the breadcrumb trail relevant?

* [Rule 12.9.1]({{< relref "./Rule-12-9-1" >}})

## Criterion 12.10

On each Web page, are groups of important links (menu, navigation bar…)
and the main content area identified (except in particular cases)?

* [Rule 12.10.1]({{< relref "./Rule-12-10-1" >}})
* [Rule 12.10.2]({{< relref "./Rule-12-10-2" >}})
* [Rule 12.10.3]({{< relref "./Rule-12-10-3" >}})
* [Rule 12.10.4]({{< relref "./Rule-12-10-4" >}})

## Criterion 12.11

On each Web page, are bypass or skip links to groups of important
links and to the main content area available (except in particular cases)

* [Rule 12.11.1]({{< relref "./Rule-12-11-1" >}})
* [Rule 12.11.2]({{< relref "./Rule-12-11-2" >}})
* [Rule 12.11.3]({{< relref "./Rule-12-11-3" >}})
* [Rule 12.11.4]({{< relref "./Rule-12-11-4" >}})

## Criterion 12.12

On each Web page, is the current page specified in the navigation menu?

* [Rule 12.12.1]({{< relref "./Rule-12-12-1" >}})

## Criterion 12.13

On each Web page, is tabbing order consistent?

* [Rule 12.13.1]({{< relref "./Rule-12-13-1" >}})
* [Rule 12.13.2]({{< relref "./Rule-12-13-2" >}})

## Criterion 12.14

On each Web page, navigation must not contain
keyboard trap. Has this rule been followed?

* [Rule 12.14.1]({{< relref "./Rule-12-14-1" >}})


