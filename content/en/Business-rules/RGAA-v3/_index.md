---
title: "RGAA v3"
date: 2020-11-22T17:14:53+02:00
weight: 60
---

## RGAA v3 business rules

* [Theme 1 - Images]({{< relref "./01.Images/" >}})
* [Theme 2 - Frames]({{< relref "./02.Frames/" >}})
* [Theme 3 - Colors]({{< relref "./03.Colours" >}})
* [Theme 4 - Multimedia]({{< relref "./04.Multimedia" >}})
* [Theme 5 - Tables]({{< relref "./05.Tables" >}})
* [Theme 6 - Links]({{< relref "./06.Links" >}})
* [Theme 7 - Script]({{< relref "./07.Scripts" >}})
* [Theme 8 - Mandatory elements]({{< relref "./08.Mandatory_elements" >}})
* [Theme 9 - Structure of information]({{< relref "./09.Structure_of_information" >}})
* [Theme 10 - Presentation of information]({{< relref "./10.Presentation_of_information" >}})
* [Theme 11 - Forms]({{< relref "./11.Forms" >}})
* [Theme 12 - Navigation]({{< relref "./12.Navigation" >}})
* [Theme 13 - Consultation]({{< relref "./13.Consultation" >}})

## RGAA v3 in Asqatasun

**173 rules** divided in:

* 118 rules of level A
* 33 rules of level AA
* 22 rules of level AAA

Among the 335 tests, 173 are implemented. So RGAA v3 in Asqatasun holds **173 rules**.

## RGAA v3 figures

* 13 themes
* 133 criteria
* 335 tests divided in:
    * 226 tests of level A
    * 48 tests of level AA
    * 61 tests of level AAA

## Links and references

For our own needs, we also created a
[diff file between AccessiWeb 2.2 and RGAA 3.0](diff_RGAA-3_AccessiWeb-2.2_AccessiWeb-HTML5-ARIA.ods), LibreOffice Calc format.

### RGAA v3

Published:  2015-04-29

* [RGAA v3](https://references.modernisation.gouv.fr/referentiel-technique-0)
* [RGAA v3 Glossary](https://references.modernisation.gouv.fr/referentiel-technique-0#title-2-glossaire)
* [RGAA v3 Particular cases](https://references.modernisation.gouv.fr/referentiel-technique-0#title-3-cas-particuliers)
* [RGAA v3 Technical notes](https://references.modernisation.gouv.fr/referentiel-technique-0#title-4-notes-techniques)
* [Release Github RGAA v3](https://github.com/DISIC/rgaa_referentiel/releases/tag/RGAA3-0) (in repos of v3.2017) Contenu = pseudo-markdown

### RGAA v3.2017

Published: 2017

* [RGAA v3.2017](http://references.modernisation.gouv.fr/rgaa/)
* [RGAA v3.2017 (2° URL)](https://references.modernisation.gouv.fr/rgaa-accessibilite/)
* [RGAA v3.2017 Glossary](http://references.modernisation.gouv.fr/rgaa/glossaire.html)
* [RGAA v3.2017 Particular cases](http://references.modernisation.gouv.fr/rgaa/cas-particuliers.html)
* [RGAA v3.2017 Technical notes](http://references.modernisation.gouv.fr/rgaa/notes-techniques.html)
* [Revision notes v3.2016 / v3.2017](http://references.modernisation.gouv.fr/rgaa/changelog.html)
* [Github repository RGAA v3.2017](https://github.com/DISIC/rgaa_referentiel)
* [Release Github RGAA v3.2017](https://github.com/DISIC/rgaa_referentiel/releases/tag/RGAA3_2017) Content = HTML + ods

### RGAA v3.2016

Published: 2016-06-23

* [RGAA v3.2016](http://references.modernisation.gouv.fr/rgaa/2016/criteres.html)
* [RGAA v3.2016 Glossary](http://references.modernisation.gouv.fr/rgaa/2016/glossaire.html)
* [RGAA v3.2016 Particular cases](http://references.modernisation.gouv.fr/rgaa/2016/cas-particuliers.html)
* [RGAA v3.2016 Technical notes](http://references.modernisation.gouv.fr/rgaa/2016/notes-techniques.html)
* [Revision notes v3 / v3.2016](http://references.modernisation.gouv.fr/rgaa/2016/changelog.html)
* [Release Github RGAA v3.2016](https://github.com/DISIC/rgaa_referentiel/releases/tag/RGAA3_2016) (in repos of v3.2017) Content = HTML + ods
