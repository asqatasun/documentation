---
title: "RGAA 3.0 Theme 7: Scripts"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Criterion 7.1

Does each script support assistive technologies, if necessary?

* [Rule 7.1.1]({{< relref "./Rule-7-1-1" >}})
* [Rule 7.1.2]({{< relref "./Rule-7-1-2" >}})
* [Rule 7.1.3]({{< relref "./Rule-7-1-3" >}})
* [Rule 7.1.4]({{< relref "./Rule-7-1-4" >}})
* [Rule 7.1.5]({{< relref "./Rule-7-1-5" >}})
* [Rule 7.1.6]({{< relref "./Rule-7-1-6" >}})

## Criterion 7.2

For each script with an alternative, is this alternative relevant?

* [Rule 7.2.1]({{< relref "./Rule-7-2-1" >}})
* [Rule 7.2.2]({{< relref "./Rule-7-2-2" >}})

## Criterion 7.3

Can each script be controlled by keyboard and mouse (except in particular cases)?

* [Rule 7.3.1]({{< relref "./Rule-7-3-1" >}})
* [Rule 7.3.2]({{< relref "./Rule-7-3-2" >}})
* [Rule 7.3.3]({{< relref "./Rule-7-3-3" >}})
* [Rule 7.3.4]({{< relref "./Rule-7-3-4" >}})

## Criterion 7.4

For each script that initiates a change of context,
is the user warned or can he control it?

* [Rule 7.4.1]({{< relref "./Rule-7-4-1" >}})

## Criterion 7.5

Can each script causing an unrequested alert be controlled
by the user (except in particular cases)?

* [Rule 7.5.1]({{< relref "./Rule-7-5-1" >}})
