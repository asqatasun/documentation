---
title: "RGAA 3.0 Theme 1: Images"
date: 2020-11-12T17:14:53+02:00
weight: 9
---

## Criterion 1.1

Does each image have a text alternative?

* [Rule 1.1.1]({{< relref "./Rule-1-1-1" >}})
* [Rule 1.1.2]({{< relref "./Rule-1-1-2" >}})
* [Rule 1.1.3]({{< relref "./Rule-1-1-3" >}})
* [Rule 1.1.4]({{< relref "./Rule-1-1-4" >}})

## Criterion 1.2

For each decorative image with a text alternative, is this alternative empty?

* [Rule 1.2.1]({{< relref "./Rule-1-2-1" >}})
* [Rule 1.2.2]({{< relref "./Rule-1-2-2" >}})
* [Rule 1.2.3]({{< relref "./Rule-1-2-3" >}})
* [Rule 1.2.4]({{< relref "./Rule-1-2-4" >}})
* [Rule 1.2.5]({{< relref "./Rule-1-2-5" >}})

## Criterion 1.3

For each image conveying information with a text alternative,
is this alternative relevant (except in particular cases)?

* [Rule 1.3.1]({{< relref "./Rule-1-3-1" >}})
* [Rule 1.3.2]({{< relref "./Rule-1-3-2" >}})
* [Rule 1.3.3]({{< relref "./Rule-1-3-3" >}})
* [Rule 1.3.4]({{< relref "./Rule-1-3-4" >}})
* [Rule 1.3.5]({{< relref "./Rule-1-3-5" >}})
* [Rule 1.3.6]({{< relref "./Rule-1-3-6" >}})
* [Rule 1.3.7]({{< relref "./Rule-1-3-7" >}})
* [Rule 1.3.8]({{< relref "./Rule-1-3-8" >}})
* [Rule 1.3.9]({{< relref "./Rule-1-3-9" >}})
* [Rule 1.3.10]({{< relref "./Rule-1-3-10" >}})

## Criterion 1.4

For each image used as CAPTCHA or as test image, with a text alternative,
does this alternative describe the nature and the purpose of the image?

* [Rule 1.4.1]({{< relref "./Rule-1-4-1" >}})
* [Rule 1.4.2]({{< relref "./Rule-1-4-2" >}})
* [Rule 1.4.3]({{< relref "./Rule-1-4-3" >}})
* [Rule 1.4.4]({{< relref "./Rule-1-4-4" >}})
* [Rule 1.4.5]({{< relref "./Rule-1-4-5" >}})
* [Rule 1.4.6]({{< relref "./Rule-1-4-6" >}})
* [Rule 1.4.7]({{< relref "./Rule-1-4-7" >}})
* [Rule 1.4.8]({{< relref "./Rule-1-4-8" >}})
* [Rule 1.4.9]({{< relref "./Rule-1-4-9" >}})

## Criterion 1.5

For each image used as CAPTCHA, is a solution for alternative access
to the content or to the purpose of the CAPTCHA available?

* [Rule 1.5.1]({{< relref "./Rule-1-5-1" >}})
* [Rule 1.5.2]({{< relref "./Rule-1-5-2" >}})

## Criterion 1.6

Does each image conveying information have a detailed description if necessary?

* [Rule 1.6.1]({{< relref "./Rule-1-6-1" >}})
* [Rule 1.6.2]({{< relref "./Rule-1-6-2" >}})
* [Rule 1.6.3]({{< relref "./Rule-1-6-3" >}})
* [Rule 1.6.4]({{< relref "./Rule-1-6-4" >}})
* [Rule 1.6.5]({{< relref "./Rule-1-6-5" >}})
* [Rule 1.6.6]({{< relref "./Rule-1-6-6" >}})
* [Rule 1.6.7]({{< relref "./Rule-1-6-7" >}})
* [Rule 1.6.8]({{< relref "./Rule-1-6-8" >}})

## Criterion 1.7

For each image conveying information with a detailed description, is this description relevant?

* [Rule 1.7.1]({{< relref "./Rule-1-7-1" >}})
* [Rule 1.7.2]({{< relref "./Rule-1-7-2" >}})
* [Rule 1.7.3]({{< relref "./Rule-1-7-3" >}})
* [Rule 1.7.4]({{< relref "./Rule-1-7-4" >}})
* [Rule 1.7.5]({{< relref "./Rule-1-7-5" >}})
* [Rule 1.7.6]({{< relref "./Rule-1-7-6" >}})
* [Rule 1.7.7]({{< relref "./Rule-1-7-7" >}})

## Criterion 1.8

When an alternate mechanism is missing, each image of text conveying information
must be replaced with styled text, if possible.
Has this rule been followed (except in particular cases)?

* [Rule 1.8.1]({{< relref "./Rule-1-8-1" >}})
* [Rule 1.8.2]({{< relref "./Rule-1-8-2" >}})
* [Rule 1.8.3]({{< relref "./Rule-1-8-3" >}})
* [Rule 1.8.4]({{< relref "./Rule-1-8-4" >}})
* [Rule 1.8.5]({{< relref "./Rule-1-8-5" >}})
* [Rule 1.8.6]({{< relref "./Rule-1-8-6" >}})

## Criterion 1.9

Each image of text conveying information must be replaced with styled text.
Has this rule been followed (except in particular cases)?

* [Rule 1.9.1]({{< relref "./Rule-1-9-1" >}})
* [Rule 1.9.2]({{< relref "./Rule-1-9-2" >}})
* [Rule 1.9.3]({{< relref "./Rule-1-9-3" >}})
* [Rule 1.9.4]({{< relref "./Rule-1-9-4" >}})
* [Rule 1.9.5]({{< relref "./Rule-1-9-5" >}})
* [Rule 1.9.6]({{< relref "./Rule-1-9-6" >}})

## Criterion 1.10

Is each image caption correctly associated with the corresponding image, if necessary?

* [Rule 1.10.1]({{< relref "./Rule-1-10-1" >}})
* [Rule 1.10.2]({{< relref "./Rule-1-10-2" >}})
* [Rule 1.10.3]({{< relref "./Rule-1-10-3" >}})
* [Rule 1.10.4]({{< relref "./Rule-1-10-4" >}})
* [Rule 1.10.5]({{< relref "./Rule-1-10-5" >}})


