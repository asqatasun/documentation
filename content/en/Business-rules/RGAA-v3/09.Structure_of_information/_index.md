---
title: "RGAA 3.0 Theme 9: Structure of information"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Criterion 9.1

On each Web page, is information structured
by the appropriate use of headings?

* [Rule 9.1.1]({{< relref "./Rule-9-1-1" >}})
* [Rule 9.1.2]({{< relref "./Rule-9-1-2" >}})
* [Rule 9.1.3]({{< relref "./Rule-9-1-3" >}})
* [Rule 9.1.4]({{< relref "./Rule-9-1-4" >}})

## Criterion 9.2

On each Web page, is the document outline coherent?

* [Rule 9.2.1]({{< relref "./Rule-9-2-1" >}})
* [Rule 9.2.2]({{< relref "./Rule-9-2-2" >}})

## Criterion 9.3

On each Web page, is each list structured  appropriately?

* [Rule 9.3.1]({{< relref "./Rule-9-3-1" >}})
* [Rule 9.3.2]({{< relref "./Rule-9-3-2" >}})
* [Rule 9.3.3]({{< relref "./Rule-9-3-3" >}})

## Criterion 9.4

On each Web page, does the first occurence
of each abbreviation help to know its meaning?

* [Rule 9.4.1]({{< relref "./Rule-9-4-1" >}})

## Criterion 9.5

On each Web page, is the meaning of each abbreviation relevant?

* [Rule 9.5.1]({{< relref "./Rule-9-5-1" >}})

## Criterion 9.6

On each Web page, is each quotation identified properly?

* [Rule 9.6.1]({{< relref "./Rule-9-6-1" >}})
* [Rule 9.6.2]({{< relref "./Rule-9-6-2" >}})

