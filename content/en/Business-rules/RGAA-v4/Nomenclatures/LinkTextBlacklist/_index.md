---
title: "LinkTextBlacklist"
date: 2020-11-14T17:44:57+01:00
---

## Purpose

* Detect non-relevant link names.
* Used by RGAAv4 rules in theme 6 (links)

## Content

English:

* `here`
* `this link`
* `there`
* `more`
* `click here`

French:

* `en savoir plus...`
* `en savoir plus`
* `ici`
* `plus`
* `là`
* `cliquez ici`
* `ce lien`
* `post précédent`
* `lire la suite`

## Link to source code

* Technical id of this nomenclature is `7`.
* [SQL code for `LinkTextBlacklist`](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/engine/asqatasun-persistence/src/main/resources/sql/automated/common/MSDB-0.0.2__insertRulesResources.sql#L108)


