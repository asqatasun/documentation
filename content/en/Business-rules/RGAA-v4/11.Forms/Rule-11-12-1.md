---
title: "Rule 11.12.1"
date: 2020-11-15T18:18:18+02:00
weight: 10
---

## Summary

This test consists in detecting `<form>` tags on the page.

The control that checks that mechanisms are provided to help to fill-in financial, juridical or personal info has to be
done manually.

## Business description

### Criterion

[11.12](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#crit-11-12)

### Test

[11.12.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/methode/criteres/#test-11-12-1)

### Description

> Pour chaque formulaire qui modifie ou supprime des données, ou qui transmet des réponses à un test ou un examen, ou
> dont la validation a des conséquences financières ou juridiques, la saisie des données vérifie-t-elle une de ces
> conditions ?
>
> * L’utilisateur peut modifier ou annuler les données et les actions effectuées sur ces données après la validation du
>     formulaire.
> * L’utilisateur peut vérifier et corriger les données avant la validation d’un formulaire en plusieurs étapes.
> * Un mécanisme de confirmation explicite, via une case à cocher (balise `<input>` de type checkbox ou balise ayant un
>     attribut WAI-ARIA role="checkbox") ou une étape supplémentaire, est présent.

### Level

AA


## Technical description

### Scope

Page

### Decision level

Semi-Decidable

## Algorithm

### Selection

#### Set1

All the `<form>` tags (form)

### Process

#### Test1

The selection handles the process.

For each occurence of the **Set1** raise a MessageA

##### MessageA: Manual check on element

* code: ManualCheckOnElements
* status: Pre-Qualified
* parameter: snippet
* present in source: yes

### Analysis

#### Not Applicable

The page has no `<form>` tag (**Set1** is empty)

#### Pre-qualified

In all other cases

## Notes

We detect the elements of the scope of the test to determine whether the test is applicable.

## Files

* [TestCases files for rule 11.12.1](https://gitlab.com/asqatasun/Asqatasun/-/tree/master/rules/rules-rgaa4.0/src/test/resources/testcases/rgaa40/Rgaa40Rule111201/)
* [Unit test file for rule 11.12.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/test/java/org/asqatasun/rules/rgaa40/Rgaa40Rule111201Test.java)
* [Class file for rule 11.12.1](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/rules/rules-rgaa4.0/src/main/java/org/asqatasun/rules/rgaa40/Rgaa40Rule111201.java)

